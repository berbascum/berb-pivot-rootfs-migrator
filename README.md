# berb-pivot-rootfs-migrator
berb-pivot-rootfs-migrator is a bash script tool that can pivot a mounted and running rootfs to a mounted tmpfs.

This is usefull when this rootfs is stored on a whole disk partition. After pivot the rootfs, is possible to take a backup of the rootfs files, and then delete the partition and repartition the disk as needed.

The script also migrates the oldroot rootfs to a new permanent local destination and reconfigures the grub bootloader.

This script has been written to be able to manage multiple migration destinations, as a second gpt partition, an lv or a luks-lvm volume.

The recently finished first stable version v1.2.0-1 has next main features (or options available as script arguments):
- pivot-root: Moves the running rootfs to a tmpfs.
- rootfs-migrate-lvm-small: Migrates the oldroot filesystem to a small logical volume using a separated boot partition for storing gruv and linux boot files. Also a partition whith the existent diskunpartitioned space will be created, but not formated.

### Usage

#### WARNING: USE THIS WITH CAUTION ON A TEST ENVIRONMENT
#### WARNING: THERE IS NOT WARRANTY
#### WARNING: THE MIGRATION FEATURE MAY ERASE ALL DISK DATA
- Copy the script to the /root dir of the Debian system to pivot and migrate.
- Connect to the system over ssh
- Execute the script with the argument "pivot-root". This will runthe pivot-stage1. The newroot structure will be created. The script will be moved from the current /root dir to /newroot.
- Once the pivot-stage1 is finished, change to /newroot dir and exec the script again (the script has been moved here while pivot-stage1 execution) with the same argument "pivot-root". This will run the pivot-stage2. oldroot will be moved to newroot.
- Exit from the ssh session and reconnect again.
- Execute the script again also with the argument "pivot-root". It will run the pivot-stage3. All running system services wil be restarted in semi-agressive mode.
- Maybe some service restart from pivot-stage3 will remain stalled. Simply Ctrl+C and rerun the script with same argument pivot-root all needed time until a message shows that pivot-stage3 is finished.
- Some dbus or systemd errors might be showed. Is normal. The goal is to be able to umount the partition which contains the mounted and running rootfs.
- Once pivot-stage3 is finished, the oldroot should be unmounted. If not, another execution of the script with the same argument pivot-root wil execute the function pivot-stage-last with additional information to try the umount.
- Once the oldroot partition has been umounted, the migration to a small lvm can be done executing the script with the argument rootfs-migrate-lvm-small

