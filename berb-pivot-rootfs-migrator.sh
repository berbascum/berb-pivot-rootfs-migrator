#!/bin/bash

# < HEADER
	BIN_ROL="main"
	SCRIPT_VERSION="1.2.1-1"
	TOOL_BRANCH='stable' 
	URGENCY="low"
# HEADER >


## Main Features:
   ## "pivot-root"
      ## Moves a running rootfs to a mounted tmpfs
   ## "rootfs-migrate-lvm-small"
      ## Migrates the oldroot pivoted rootfs to a small lv using a separated boot partition.


# Upstream-Name: berb-pivot-rootfs-migrator
# Source: https://gitlab.com/berbascum/berb-pivot-rootfs-migrator
# Source: https://git.ticv.cat/berbascum/berb-pivot-rootfs-migrator
  ## Berbscum's script to pivot a running rootfs to a mounted tmpfs and migrate it to some local device

# Copyright (C) 2023 Berbascum <berbascum@ticv.cat>
# All rights reserved.

# BSD 3-Clause License
#
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the <organization> nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

NEWROOT_DIR="/newroot"
START_DIR=$(pwd)
SCRIPT_NAME=$(echo $0 | awk -F'/' '{print $NF}')

## Enable sudo if user not root
if [ "$USER" != 'root' ]; then SUDO='sudo'; fi

## NOTA: Realitzar el procés connectant per ssh.

fn_pivot_stage1() {
	echo && echo "Iniciant pivot stage1..."
	echo && echo "Aturant serveis prescindibles..."
	arr_SERVEIS_STOP=( "cron" "chrony" "chronyd" "qemu-guest-agent" )
	for servei in ${arr_SERVEIS_STOP[@]}; do
		echo && echo "Aturant servei \"${servei}\"..."
		systemctl stop "${servei}" 2>/dev/null
	done

	## Instal·lar al rootfs inicial:
	echo && echo "Actualitzant repositoris apt..."
	apt-get update > /dev/null
	echo && echo "apt-get update finalitzat: $?"
	echo && echo "Instal·lant software adicional..."
	apt-get -y install parted lsof uuid-runtime lvm2 cryptsetup curl rsync >/dev/null
	echo && echo "apt-get install extra soft finalitzat: $?"

	## desmuntar boot
	echo && echo "Desmuntant particions de \"boot\"..."
	umount -v /boot/efi
	umount -v /boot

	### Crear tmpfs per al temproot
	echo && echo "Creant \"newroot\"..."
	mkdir "${NEWROOT_DIR}"
	mount -v -t tmpfs none "${NEWROOT_DIR}"
	mkdir "${NEWROOT_DIR}"/{proc,sys,dev,run,usr,var,tmp,oldroot}
	echo && echo "Copiant arxius de systema mínim a \"newroot\"..."
	cp -ax /{bin,etc,mnt,sbin,lib,lib64} "${NEWROOT_DIR}"
	cp -ax /usr/{bin,sbin,lib,lib64} "${NEWROOT_DIR}"/usr/
	cp -ax /var/{lib,local,lock,opt,run,spool,tmp} "${NEWROOT_DIR}"/var/

	## Remuntar "/" en mode privat (--make-rprivate)
	   # #(si està en mode shared, que sol ser per defecte de systemd (--make-sdhared), el pivot_root fallarà)
	echo && echo "Remuntant \"/\" en mode privat..."
	mount --make-rprivate /
	## COPIA script a arrel de newroot
	echo && echo "Copiat aquest script a \"${NEWROOT_DIR}\""
	cp -av "${START_DIR}/${SCRIPT_NAME}" "${NEWROOT_DIR}"
	rm -v "${START_DIR}/${SCRIPT_NAME}"
	## Crea arxiu pivot_stage1 a newroot
	echo "done" > "${NEWROOT_DIR}/pivot_stage1"
	echo && echo "Finalitzat pivot stage1"
	echo && echo && echo "IMPORTANT: Canviar al dir ${NEWROOT_DIR} i executar de nou l'script!"
	echo
}


fn_pivot_stage2() {
	echo && echo "Iniciant pivot stage2..."
	## Pivot_root: 
	   ## pivot_root newroot put_old
	      ## put_old és un dir dins del newroot on estarà muntat el oldroot.

	## Desmuntar part sxx15 efi,
	umount -v /boot/efi

	#cd "${NEWROOT_DIR}"
	echo && echo "Executant: \"pivot_root ${NEWROOT_DIR} ${NEWROOT_DIR}/oldroot\"..."
	pivot_root "${NEWROOT_DIR}" "${NEWROOT_DIR}"/oldroot
	## IMPORTANT: Canviar a arrel
	cd /
	## Moure els dirs de memòria de l'oldroot al newroot
	echo && echo "Movent \"dev sys run proc\" de \"oldroot\" a \"newroot\"..."
	for i in dev proc sys run; do mount --move /oldroot/$i /$i; done
	## Reiniciar ssh
	echo && echo "Reiniciant \"ssh\""
	systemctl restart sshd
	echo "done" > "/pivot_stage2"
	echo && echo "Finalitzat pivot stage2"
	echo && echo && echo "* IMPORTANT * Reconnectar sessió ssh i executar script de nou"
	echo
}

fn_pivot_stage3() {
	echo && echo "Iniciant pivot stage3..."

	## Desmuntar part sxx15 efi,
	umount -v /boot/efi

	## Reinicia més serveis
	# arr_SERVEIS_RESTART=( "syslog" "systemd-udevd" "systemd-journald" "getty@tty1" \
	  # "serial-getty@ttyS0" "user@0" "systemd-logind" )
	echo && echo "Reiniciant 1 serveis en execució:"
	COMANDA=$(systemctl list-units --type=service --state=running | grep running | awk '{print $1}')
	for running_service in ${COMANDA}; do
		echo && echo && echo "Reiniciant servei \"${running_service}\"..."
		echo && echo "If service restart remains stalled, pres \"Ctrl +C\" and rerun the script with arg \" pivot-root \""
		systemctl restart "${running_service}"
        done

	## Recarrega systemd
	echo && echo "Reload \"systemd\"..."
	systemctl daemon-reload
	
	## Reinicia de nou debus complert
	echo && echo "Reiniciant \"dbus\"..."
	systemctl restart dbus
	systemctl restart dbus.socket
	systemctl restart dbus

	## Recarrega systemd
	# echo && echo "Reload \"systemd\"..."
	# systemctl daemon-reload

	## Kill "unattended-upgr" proceses
	echo && echo "Matant procés \"unattended-upgr\"..."
	PS_2_KILL=$(ps aux | grep unattended-upgr | grep --invert-match "grep" | awk '{print $2}')
	kill ${PS_2_KILL}
	# Kill (ps) "(sd-pam"
	echo && echo "Matant procés \"sd-pam\"..."
	PS_2_KILL=$(ps aux | grep sd-pam| grep --invert-match "grep" | awk '{print $2}')
	kill ${PS_2_KILL}

	## Ara la sortida de fuser -vm /oldroot
	# Només hauria de mostrar el krnl_mount i systemd apm PID1
	
	# Matar sense -9 el procés 1
	## Matant procés 1 de systemd
	echo && echo "Matant procés \"1\" de \"systemd\"..."
	kill 1
	## Recarregar de nou systemd
	echo && echo "Reload \"systemd\"..."
	systemctl daemon-reload
	
	## Intentar umount /oldroot
	echo && echo "Intentant desmuntar /oldroot..."
	sync
	sleep 5
	umount -v /oldroot
	sleep 5
	## Desmuntar part sxx15 efi,
	   ## doncs es remunta amb "systemctl isolate default"
	umount -v /boot/efi

	echo && echo "Reiniciant 2 serveis en execució:"
	COMANDA=$(systemctl list-units --type=service --state=running | grep running | awk '{print $1}')
	for running_service in ${COMANDA}; do
		echo && echo && echo "Reiniciant servei \"${running_service}\"..."
		echo && echo "If service restart remains stalled, pres \"Ctrl +C\" and rerun the script with arg \" pivot-root \""
		systemctl restart "${running_service}"
        done
	echo "done" > "/pivot_stage3"

	echo && echo "ERRORS: Si falla el desmuntatge d'oldroot executar de nou l'script amb arg \"pivot-root\""
	echo && echo "Un cop desmuntat \"/oldroot\" amb èxit:"
	        echo "	 - NO PERMET resize2fs ni parted resize"
		echo "	 - SI PERMET fdisk, parter rm"
	echo && echo "Per a fer la migració a lv no crypt, executar script amb arg \" rootfs-migrate-lvm-small \""
	echo && echo "Finalitzat pivot stage3"
	echo && echo
}

fn_systemd_agresive_reload() {
	## Passar a mode rescat per a aturar major part dels serveis, inclou ssh
	echo && echo "Sortir de la sessió ssh i accedir per consola directa al servidor, doncs ssh s'aturarà i no es podrà reconnectar!"
	echo "EXECUTAR MANUALMENT mode \"rescue\":"
	echo "    \"systemctl isolate rescue\""
	echo && echo "Entrar root pass (NO Ctrl+D)"
	echo "Tornar al mode \"default\":"
	echo "    \"systemctl isolate rescue\""
	echo && echo "Ctr+C si queda encallat!"
	echo "Logout terminal consola directa del servidor"
	echo && echo "Reconnectar de nou per ssh"
}

fn_pivot_stage_last() {
	echo && echo "Iniciant pivot stage5..."
	## Recarregar de nou systemd
	echo && echo "Reload \"systemd\"..."
	systemctl daemon-reload
	sync
	sleep 1

	echo && echo "Reiniciant serveis:"
	COMANDA=$(systemctl list-units --type=service --state=running | grep running | awk '{print $1}')
	for running_service in ${COMANDA}; do
		echo && echo "Reiniciant servei \"${running_service}\"..."
		systemctl restart "${running_service}"
        done


	## Intentar umount /oldroot
	echo && echo "Intent 2 de desmuntar /oldroot..."
	umount -v /oldroot

	## Desmuntar part sxx15 efi,
	   ## doncs es remunta amb "systemctl isolate default"
	# umount -v /boot/efi

	echo && echo "ERRORS: Si falla el desmuntatge, verificar si ha quedat algun procés penjat a oldroot:"
	echo "    \"fuser -vm /oldroot"
	echo && echo "Matar el procés o processos amb:"
        echo "    \"kill -9\""
	echo && echo && echo "Finalitzat pivot stage5"
	echo && echo && echo "QUAN UMOUNT OLDROOT OK EXECUTAR:"
       	echo "pivot_script arg: \"rootfs-conf\" per a configurar el rootfs de part2 i el boot!!!"
	echo && echo "També podeu provar el reload agressiu de systemd següint les següents instruccions:"
	fn_systemd_agresive_reload
}

fn_chroot() {
## Usage info:
	#echo && echo "Configurant chroot a lv-rootfs..."
	## Iniciant chroot
	#echo && echo "Iniciant chroot a lv-rootfs i reinstal·lant grub a sda..."
	#fn_chroot start "'grub-install --no-floppy' ${DISK_DEV}"
	## Aturant chroot
	#echo && echo "Aturant chroot a lv-rootfs..."
	#fn_chroot stop

	if [ "$1" = "start" ]; then
		${SUDO} mount -o bind /dev ${CHROOT_DIR}/dev
		${SUDO} mount -o bind /dev/pts ${CHROOT_DIR}/dev/pts
		${SUDO} mount -o bind /proc ${CHROOT_DIR}/proc
		${SUDO} mount -o bind /sys ${CHROOT_DIR}/sys
		# ${SUDO} chroot $CHROOT_DIR /bin/bash
		[ -n "$2" ] && "${SUDO}" chroot "${CHROOT_DIR}" "$2"
	elif [ "$1" = "stop" ]; then
		${SUDO} umount ${CHROOT_DIR}/sys
		${SUDO} umount ${CHROOT_DIR}/proc
		${SUDO} umount ${CHROOT_DIR}/dev/pts
		${SUDO} umount ${CHROOT_DIR}/dev
		# umount ${CHROOT_DIR}
	 fi
}

fn_get_disk_info_get_primary_vars() {
	echo && echo "Setting disk info primery vars..."
	echo
	## Creant arxiu /disk-info.sh
	echo "#!/bin/bash" > "/disk-info.sh"
	echo "echo && echo  \"Loading disk info from file \\\"/disk-info.sh\\\"\"" >> "/disk-info.sh"
	echo "echo" >> "/disk-info.sh"
	## Escrivint var defs a /disk-info.sh
	OLDROOT_FS_UUID=$(for i in $(cat /proc/cmdline); do echo "$i" | grep "root=UUID=" | awk -F'=' '{print $3}'; done)
	echo "OLDROOT_FS_UUID=\"${OLDROOT_FS_UUID}\"" >> "/disk-info.sh"
	OLDROOT_DEV_NAME=$(ls -l /dev/disk/by-uuid/${OLDROOT_FS_UUID} | awk -F'/' '{print $NF}' | sed 's/[^a-z]*//g')
	echo "OLDROOT_DEV_NAME=\"${OLDROOT_DEV_NAME}\"" >> "/disk-info.sh"
	OLDROOT_DEV_PATH="/dev/${OLDROOT_DEV_NAME}"
	echo "OLDROOT_DEV_PATH=\"${OLDROOT_DEV_PATH}\"" >> "/disk-info.sh"
	OLDROOT_DEV_PARTNUM=$(ls -l /dev/disk/by-uuid/${OLDROOT_FS_UUID} | awk -F'/' '{print $NF}' | sed 's/[^0-9]*//g')
	echo "OLDROOT_DEV_PARTNUM=\"${OLDROOT_DEV_PARTNUM}\"" >> "/disk-info.sh"
	OLDROOT_DEV_FULL_PATH="${OLDROOT_DEV_PATH}${OLDROOT_DEV_PARTNUM}"
	echo "OLDROOT_DEV_FULL_PATH=\"${OLDROOT_DEV_FULL_PATH}\"" >> "/disk-info.sh"
	OLDROOT_MOUNT_POINT="/mnt/${OLDROOT_DEV_NAME}${OLDROOT_DEV_PARTNUM}"
	echo "OLDROOT_MOUNT_POINT=\"${OLDROOT_MOUNT_POINT}\"" >> "/disk-info.sh"
	## Permisos x disk-info.sh
	chmod 700 "/disk-info.sh"
}

fn_get_disk_info_print_vars() {
## Prints disk-info.sh lines containing "=" as vars information on screen
	echo
	while read line; do
		echo "${line}" | grep '='
	done < "./disk-info.sh"
}

fn_get_disk_info() {
	## Get rootfs disk info
	
	## Check for /disk-info.sh
	if [ -e "/disk-info.sh" ]; then
		echo && read -p "\"/disk-info.sh\" file found. Load o Recreate? [ l | c ]: " RESPOSTA
		case ${RESPOSTA} in
			l|L|load|LOAD|Load|lOAD)
				## Load disk info primary vars from disk-info.sh
				source "/disk-info.sh"
				## Print vars defined on disk-info.sh
				fn_get_disk_info_print_vars
				;;
			*)
				## Load disk info primary vars scanning the disk
				fn_get_disk_info_get_primary_vars
				## Print vars defined on disk-info.sh
				fn_get_disk_info_print_vars
				;;
		esac
	else
		## Load disk info primary vars scanning the disk
		fn_get_disk_info_get_primary_vars
		## Print vars defined on disk-info.sh
		fn_get_disk_info_print_vars
	fi

	## Si no disk detectat, exit
	[ -z "${OLDROOT_DEV_NAME}" ] && echo && echo "Can not find the rootfs disk device name!" && exit 1

	## Get disk size information
	DISK_SIZE_WITH_UNIT=$(parted "${OLDROOT_DEV_PATH}" print \
		| grep "Disk ${OLDROOT_DEV_PATH}" \
		| awk '{print $3}')
	DISK_SIZE_NUMERIC=$(echo "${DISK_SIZE_WITH_UNIT}" \
		| sed 's/[^0-9]*//g')

	DISK_SIZE_UNIT=$(echo "${DISK_SIZE_WITH_UNIT}" \
		| sed 's/[^A-Z]*//g')
	## Si el tamany de disc està en GB, ho passem a MB
	[ "${DISK_SIZE_UNIT}" == "GB" ] && DISK_SIZE_NUMERIC_MB=$((${DISK_SIZE_NUMERIC}*1024))
	[ "${DISK_SIZE_UNIT}" == "MB" ] && DISK_SIZE_NUMERIC_MB=${DISK_SIZE_NUMERIC}
	## Get part table info
	DISK_PART_TBL=$(parted "${OLDROOT_DEV_PATH}" print \
		| grep "Partition Table" \
		|  awk '{print $3}')

	## Get rootfs part START size information
	OLDROOT_PART_NAME_PARTED="oldroot"
	OLDROOT_PART_START_WITH_UNIT=$(parted "${OLDROOT_DEV_PATH}" print \
		| grep --invert-match "Disk ${OLDROOT_DEV_PATH}" \
		| grep "${DISK_SIZE_WITH_UNIT}" \
		| awk '{print $2}')
	OLDROOT_PART_START_NUMERIC=$(echo "${OLDROOT_PART_START_WITH_UNIT}" \
		| sed 's/[^0-9]*//g')
	OLDROOT_PART_START_UNIT=$(echo "${OLDROOT_PART_START_WITH_UNIT}" \
		| sed 's/[^A-Z]*//g')
	# echo && echo "OLDROOT_PART_START_NUMERIC = ${OLDROOT_PART_START_NUMERIC}" #DEBUG
	# echo && echo "OLDROOT_PART_START_UNIT = ${OLDROOT_PART_START_UNIT}" #DEBUG
	echo && echo "OLDROOT_PART_START_WITH_UNIT = ${OLDROOT_PART_START_WITH_UNIT}" #DEBUG
	
	## Get rootfs part END size information ## Not used END size, using END %
	# OLDROOT_PART_END_WITH_UNIT=$(parted "${OLDROOT_DEV_PATH}" print \
	#	| grep --invert-match "Disk ${OLDROOT_DEV_PATH}" \
	#	| grep "${DISK_SIZE_WITH_UNIT}" \
	#	| awk '{print $3}')
	# OLDROOT_PART_END_NUMERIC=$(echo "${OLDROOT_PART_END_WITH_UNIT}" \
	#	| sed 's/[^0-9]*//g')
	# OLDROOT_PART_END_UNIT=$(echo "${OLDROOT_PART_END_WITH_UNIT}" \
	#	| sed 's/[^A-Z]*//g')
	# [ "${OLDROOT_PART_END_UNIT}" == "GB" ] && OLDROOT_PART_END_NUMERIC_MB=$((${OLDROOT_PART_END_NUMERIC}*1024))
	# [ "${OLDROOT_PART_END_UNIT}" == "MB" ] && OLDROOT_PART_END_NUMERIC_MB=${OLDROOT_PART_END_NUMERIC}
	OLDROOT_PART_MIN_NEW_SIZE_MB="4000" ## És el tamany mínim, que es passarà a % i s'afegirà un 1%
	echo && echo "OLDROOT_PART_MIN_NEW_SIZE_MB ${OLDROOT_PART_MIN_NEW_SIZE_MB}" #DEBUG
	OLDROOT_PART_END_NUMERIC=$((${OLDROOT_PART_START_NUMERIC} + ${OLDROOT_PART_MIN_NEW_SIZE_MB}))
	# OLDROOT_PART_END_WITH_UNIT="${OLDROOT_PART_END_NUMERIC}MB"
	# echo && echo "OLDROOT_PART_END_WITH_UNIT ${OLDROOT_PART_END_WITH_UNIT}" #DEBUG

	## Es busca % de new size part1 respecte disk size, i es suma 1% per a assegurar arrodonir a l'alça i no quedar curt
	OLDROOT_PART_END_PERCENT_NUMERIC=$(((${OLDROOT_PART_END_NUMERIC} * 100 / ${DISK_SIZE_NUMERIC_MB}) + 1))
	OLDROOT_PART_END_PERCENT_WITH_UNIT="${OLDROOT_PART_END_PERCENT_NUMERIC}%"
	echo && echo "OLDROOT_PART_END_PERCENT_WITH_UNIT = ${OLDROOT_PART_END_PERCENT_WITH_UNIT}" #DEBUG

	## Get boot info
	## efi part
	PART_NAME="efi"
	BOOT_EFI_PART_COUNT=$(cat /etc/fstab | grep -c "\/boot\/efi")
	if [ "${BOOT_EFI_PART_COUNT}" -ge "2" ]; then
		echo && echo "Many \"${PART_NAME}\" part found in fstab: \"${BOOT_EFI_PART_COUNT}\" !!!"
		exit 1
	elif [ "${BOOT_EFI_PART_COUNT}" -eq "1" ]; then
		echo && echo "Detected a \"${PART_NAME}\" partition in fstab!"
		## var BOOT_EFI_PART_FS_UUID
		## Si la var no és definida, vol dir que s'ha no s'fet load de disk-info.sh i es defineix generanu un nou uuid
		if [ -z "${BOOT_EFI_PART_FS_UUID}" ]; then 
			BOOT_EFI_PART_FS_UUID=$(cat /etc/fstab | grep "\/boot\/efi" \
				| awk '{print $1}' | awk -F'=' '{print $2}')
			echo "BOOT_EFI_PART_FS_UUID=\"${BOOT_EFI_PART_FS_UUID}\"" >> "/disk-info.sh"
		fi
		BOOT_EFI_PART_DEV_NAME=$(ls -l /dev/disk/by-uuid/${BOOT_EFI_PART_FS_UUID} \
			| awk -F'/' '{print $NF}' | sed 's/[^a-z]*//g')
		BOOT_EFI_PART_DEV_PARTNUM=$(ls -l /dev/disk/by-uuid/${BOOT_EFI_PART_FS_UUID} \
			| awk -F'/' '{print $NF}' | sed 's/[^0-9]*//g')
		BOOT_EFI_DEV_FULL_PATH="/dev/${BOOT_EFI_PART_DEV_NAME}${BOOT_EFI_PART_DEV_PARTNUM}"
		echo "Partició efi: ${BOOT_EFI_PART_DEV_NAME}${BOOT_EFI_PART_DEV_PARTNUM}" #DEBUG
		echo "BOOT_EFI_PART_FS_UUID = ${BOOT_EFI_PART_FS_UUID}" #DEBUG
	elif [ "${BOOT_EFI_PART_COUNT}" -eq "0" ]; then
		echo && echo "Not detected a \"${PART_NAME}\" partition in fstab!"
	fi
	## grub part 
	PART_NAME="boot"
	BOOT_GRUB_PART_NAME_PARTED="bootgrub"
	BOOT_GRUB_PART_COUNT=$(cat /etc/fstab | grep "\/boot" | grep -c --invert-match "\/boot\/efi")
		#| awk '{print $1}' | awk -F'=' '{print $2}')
	if [ "${BOOT_GRUB_PART_COUNT}" -ge "2" ]; then
		echo "Many \"${PART_NAME}\" partitions found in fstab: \"${BOOT_GRUB_PART_COUNT}\" !!!"
		exit 1
	elif [ "${BOOT_GRUB_PART_COUNT}" -eq "1" ]; then
		echo "Existing \"${PART_NAME}\" part in fstab not implemented yet!"
		exit 1
	elif [ "${BOOT_GRUB_PART_COUNT}" -eq "0" ]; then
		echo && echo "\"${PART_NAME}\" part not found in fstab. It will be created!!"

		## var BOOT_GRUB_PART_FS_UUID
		## Generating uuid for bootgrub partition FS
		## Si la var no és definida, vol dir que s'ha no s'fet load de disk-info.sh i es defineix generanu un nou uuid
		if [ -z "${BOOT_GRUB_PART_FS_UUID}" ]; then 
			BOOT_GRUB_PART_FS_UUID=$(uuidgen -r)
			echo "BOOT_GRUB_PART_FS_UUID=\"${BOOT_GRUB_PART_FS_UUID}\"" >> "/disk-info.sh"
		fi

		## Set bootgrub part end "oldroot_% * 2" getting the new part with same % as oldroot
		BOOT_GRUB_PART_START_PERCENT_WITH_UNIT="${OLDROOT_PART_END_PERCENT_WITH_UNIT}"
		BOOT_GRUB_PART_END_PERCENT_WITH_UNIT="$((${OLDROOT_PART_END_PERCENT_NUMERIC} * 2))%"
		## +1 bootgrub	+2 [lvm-small|lvm-full]	  +3 (if 2 = lvm-small) lvm-full
		BOOT_GRUB_DEV_PARTNUM=$((${OLDROOT_DEV_PARTNUM} + 1))
		BOOT_GRUB_DEV_FULL_PATH="${OLDROOT_DEV_PATH}${BOOT_GRUB_DEV_PARTNUM}"
		BOOT_GRUB_TMP_MOUNT_POUINT="/mnt/boot-tmp"
		echo
		echo "BOOT_GRUB_PART_FS_UUID = ${BOOT_GRUB_PART_FS_UUID}" #DEBUG
		echo "BOOT_GRUB_DEV_PARTNUM = ${BOOT_GRUB_DEV_PARTNUM}" #DEBUG
		echo "BOOT_GRUB_PART_START_PERCENT_WITH_UNIT = ${BOOT_GRUB_PART_START_PERCENT_WITH_UNIT}" #DEBUG
		echo "BOOT_GRUB_PART_END_PERCENT_WITH_UNIT = ${BOOT_GRUB_PART_END_PERCENT_WITH_UNIT}" #DEBUG
	fi

	## Get newroot part gpt information
	NEWROOT_PART_NAME_PARTED="${NEWROOT_PART_MODE_OPTION}"

	## var BOOT_GRUB_PART_FS_UUID
	## Generating uuid for bootgrub partition FS
	## Si la var no és definida, vol dir que s'ha no s'fet load de disk-info.sh i es defineix generanu un nou uuid
	if [ -z "${NEWROOT_FS_UUID}" ]; then 
		NEWROOT_FS_UUID="$(uuidgen -r)"
		echo "NEWROOT_FS_UUID=\"${NEWROOT_FS_UUID}\"" >> "/disk-info.sh"
	fi

	echo && echo "NEWROOT_FS_UUID definitiu = ${NEWROOT_FS_UUID}" #DEBUG
	## +1 bootgrub	+2 [lvm-small|lvm-full]	  +3 (if 2 = lvm-small) lvm-full
	NEWROOT_DEV_PARTNUM=$((${OLDROOT_DEV_PARTNUM} + 2))
	echo "NEWROOT_DEV_PARTNUM = ${NEWROOT_DEV_PARTNUM}" #DEBUG
	NEWROOT_PART_START_PERCENT_WITH_UNIT="${BOOT_GRUB_PART_END_PERCENT_WITH_UNIT}"
	echo "NEWROOT_PART_START_PERCENT_WITH_UNIT = ${NEWROOT_PART_START_PERCENT_WITH_UNIT}" #DEBUG
	## Configuring var: NEWROOT_PART_END_PERCENT_WITH_UNIT
	   ## Si "mode=lvm-small" es creara newroot part small amb lvm on top.
	      ## La fn crea parts, si el mode és lvm-small, crearà una altra part amb 100%FREE 
	   ## Si "mode=lvm-full" es creara newroot part amb lvm on top 100%FREE
	      ## La corresponent funció haurà de configurar-ho com a lvm i no com a luks amb lvm
	   ## Si "mode=luks-full" es creara luks part amb lvm on top 100%FREE
	      ## La corresponent funció haurà de configurar-ho com a luks amb lvm on top
	if [ "${NEWROOT_PART_MODE_OPTION}" == "lvm-small" ]; then
		## Set newroot part end "oldroot_% * 3" getting the new part with same % as oldroot and bootgrub parts
		NEWROOT_PART_END_PERCENT_WITH_UNIT="$((${OLDROOT_PART_END_PERCENT_NUMERIC} * 3))%"
	elif [[ "${NEWROOT_PART_MODE_OPTION}" == +(lvm-full|luks-full) ]]; then
		NEWROOT_PART_END_PERCENT_WITH_UNIT="100%"
	fi
	echo "NEWROOT_PART_END_PERCENT_WITH_UNIT = ${NEWROOT_PART_END_PERCENT_WITH_UNIT}" #DEBUG

	## Get luks part gpt information
	if [ "${NEWROOT_PART_MODE_OPTION}" == "lvm-small" ]; then
		echo && echo "Loading config for extra luks part as \"${NEWROOT_PART_MODE_OPTION}\" mode is used..."
		LUKS_PART_NAME_PARTED="luks"
		LUKS_PART_START_PERCENT_WITH_UNIT="${NEWROOT_PART_END_PERCENT_WITH_UNIT}"
		LUKS_PART_END_PERCENT_WITH_UNIT="100%"
	fi
}

fn_partitions_recreation() {
	# Crear tar del rootfs original de part1
	echo && echo "Preparant creació de tar del rootfs original de \"${OLDROOT_DEV_NAME}${OLDROOT_DEV_PARTNUM}\"..."
	mkdir -v "${OLDROOT_MOUNT_POINT}"
	mount -v "${OLDROOT_DEV_PATH}${OLDROOT_DEV_PARTNUM}" "${OLDROOT_MOUNT_POINT}"
	echo && echo "Creant tar..."
	tar zcf /rootfs-backup.tar.gz -C "${OLDROOT_MOUNT_POINT}" --one-file-system --exclude="newroot" ./
	echo "Resultat tar: $?"
	umount -v "${OLDROOT_MOUNT_POINT}"
	sync
	sleep 2

	## Creació noves particions
	echo " " && echo "Removing partition \"oldroot\"..."
	parted -s "${OLDROOT_DEV_PATH}" rm ${OLDROOT_DEV_PARTNUM} #2>/dev/null
	sleep 1
	# if [ "${BOOT_GRUB_PART_COUNT}" -eq "1" ]; then
		#  TODO
	# fi

	## Recreate oldroot partition
	echo " " && echo "Recreating partition "${OLDROOT_PART_NAME_PARTED}" més petita..."
	parted -s -a optimal "${OLDROOT_DEV_PATH}" mkpart "${OLDROOT_PART_NAME_PARTED}" ext4 \
		"${OLDROOT_PART_START_WITH_UNIT}" "${OLDROOT_PART_END_PERCENT_WITH_UNIT}" # oldroot
	sleep 1
	## Create bootbrub partition
	echo " " && echo "Creating partition \"${BOOT_GRUB_PART_NAME_PARTED}\"..."
	parted -s -a optimal "${OLDROOT_DEV_PATH}" mkpart "${BOOT_GRUB_PART_NAME_PARTED}" ext2 \
		"${BOOT_GRUB_PART_START_PERCENT_WITH_UNIT}" "${BOOT_GRUB_PART_END_PERCENT_WITH_UNIT}" # bootgrub
	sleep 2
	echo && echo "Creeating FS with custom UUID on \"bootgrub\" part..."
	mke2fs -t ext2 -U "${BOOT_GRUB_PART_FS_UUID}" "${BOOT_GRUB_DEV_FULL_PATH}" #>/dev/null
	sleep 1

	## Create lvm partition # depenent si = lvm-full o lvm-small, es defineixen les vars start i end
	echo " " && echo "Creating partition \""${NEWROOT_PART_NAME_PARTED}"\"..."
	parted -s -a optimal "${OLDROOT_DEV_PATH}" mkpart "${NEWROOT_PART_NAME_PARTED}" ext4 \
		"${NEWROOT_PART_START_PERCENT_WITH_UNIT}" "${NEWROOT_PART_END_PERCENT_WITH_UNIT}" # newroot lvm o luks
	sleep 1

	## Create luks 100% Free partition if newroot has creted with lvm-small mode
	if [ "${NEWROOT_PART_MODE_OPTION}" == "lvm-small" ]; then
		echo && echo "Creant partició \""${LUKS_PART_NAME_PARTED}"\"..."
		parted -s -a optimal "${OLDROOT_DEV_PATH}" mkpart "${LUKS_PART_NAME_PARTED}" ext4 \
			"${LUKS_PART_START_PERCENT_WITH_UNIT}" "${LUKS_PART_END_PERCENT_WITH_UNIT}" # newroot lvm o luks
		sleep 1
	fi
}

fn_lvm2_config_vars() {
	## lvm config
	echo && echo "Loading lvm2 config vars..." #DEBUG
	VG_NAME="vg-ada"
	LV_ROOTFS_NAME="lv-rootfs"
	LV_DEV_MAPPER_PATHNAME="/dev/mapper/vg--ada-lv--rootfs"
	LV_DEV_VG_FULL_PATH="/dev/${VG_NAME}/${LV_ROOTFS_NAME}"
	NEWROOT_MOUNT_POINT='/mnt/lv-rootfs'
	NEWROOT_DEV_FULL_PATH=${LV_DEV_VG_FULL_PATH}
		[ ! -d "${NEWROOT_MOUNT_POINT}" ] && mkdir "${NEWROOT_MOUNT_POINT}"
	CHROOT_DIR="${NEWROOT_MOUNT_POINT}"
	## Get part lvm information
	## Set lv size value resting 5% to part1 size value to grant enough space for dding lv to part1 for repairing
	LV_NEW_SIZE_NUMERIC=$((${OLDROOT_PART_MIN_NEW_SIZE_MB} - (${OLDROOT_PART_MIN_NEW_SIZE_MB} * 5 / 100)))
	LV_NEW_SIZE_WITH_UNIT=$(echo "${LV_NEW_SIZE_NUMERIC}M")
	echo && echo "LV_NEW_SIZE_WITH_UNIT = ${LV_NEW_SIZE_WITH_UNIT}" #DEBUG
}

fn_lvm2_creation() {
	## Creació lvm2
	echo && echo "Configuring LVM..."
	pvcreate "${OLDROOT_DEV_PATH}${NEWROOT_DEV_PARTNUM}"
	vgcreate "${VG_NAME}" "${OLDROOT_DEV_PATH}${NEWROOT_DEV_PARTNUM}"
	lvcreate -n "${LV_ROOTFS_NAME}" -L "${LV_NEW_SIZE_WITH_UNIT}" "${VG_NAME}"
	## mkfs es fa a la fn del repair, doncs es formata amb custom UUID de oldroot
}

fn_lvm2_get_lvmids() {
	# Get fsuuid, vgid and lvid
	echo && echo "Getting VG and LV ids..."
	VG_UUID=$(vgdisplay | grep "VG UUID" | awk '{print $3}')
	LV_UUID=$(lvdisplay | grep "LV UUID" | awk '{print $3}')
}

fn_oldroot_part_repair() {
	## Formata lv|part amb UUID custom.
	echo && echo "Formatant lv amb UUID custom..."
	mke2fs -t ext4 -U "${OLDROOT_FS_UUID}" "${NEWROOT_DEV_FULL_PATH}" >/dev/null
	## Restaurar rootfs original a lv
	echo && echo "Muntant lv ..."
	mount -v "${NEWROOT_DEV_FULL_PATH}" "${NEWROOT_MOUNT_POINT}"
	echo
	echo "Extraient tar oldroot \"${OLDROOT_DEV_NAME}${OLDROOT_DEV_PARTNUM}\" a \"${NEWROOT_MOUNT_POINT}\"..."
	tar zxC "${NEWROOT_MOUNT_POINT}" -f /rootfs-backup.tar.gz
	echo "Resultat tar: $?"
	echo
	sync && umount -v "${NEWROOT_MOUNT_POINT}"
	sleep 1
	## Reparació filesystem part1 utilitzant dd des de lv
	echo && echo "Reparant \"${OLDROOT_DEV_NAME}${OLDROOT_DEV_PARTNUM}\" utilitzant \"${NEWROOT_DEV_FULL_PATH}\":"
	echo && echo "Flash dd a \"${OLDROOT_DEV_NAME}${OLDROOT_DEV_PARTNUM}\"..."
	dd if="${NEWROOT_DEV_FULL_PATH}" of="${OLDROOT_DEV_PATH}${OLDROOT_DEV_PARTNUM}" status=progress
	sync
	sleep 3
}

fn_newroot_conf_uuid() {
	## Configura FS UUID definitiu a newroot lv/part2
	echo && echo "Configurant FS UUID definitiu a \"${NEWROOT_DEV_FULL_PATH}\"..."
	echo && echo "Verificant FS a \"${NEWROOT_DEV_FULL_PATH}\"..."
	e2fsck -f -y "${NEWROOT_DEV_FULL_PATH}" >/dev/null
	echo && echo "Canviant UUID pel definitiu a \"${NEWROOT_DEV_FULL_PATH}\"..."
	yes | tune2fs -U "${NEWROOT_FS_UUID}" "${NEWROOT_DEV_FULL_PATH}" >/dev/null
	echo && echo "Verificant FS a \"${NEWROOT_DEV_FULL_PATH}\"..."
	e2fsck -f -y "${NEWROOT_DEV_FULL_PATH}" >/dev/null
}

fn_rootfs_migrate_lvm_small() {
## El boot quedarà a bootgrub part
   ## Com que copio un grub.cfg custom, no caldria fer update-grub2
   ## Com que hi ha partició boot separada, caldrà reinstal·lar grub amb chroot
## El rootfs quedarà a lv-rootfs
## Es crea newroot part >4G
## Es crea lv <4000M on es descomprimeix rootfs tar amb UUID de oldroot, i amb dd es repararà oldroot part.
## Després es configura el boot de lv-rootfs

	## Size mode for lvm2 physycal volume part creation
	echo && echo && echo
	echo "################################################################"
	echo "#### WARNING: THE MIGRATION FEATURE MAY ERASE ALL DISK DATA ####"
	echo "################################################################"
	echo "#### WARNING:  USE THIS WITH CAUTION ON A TEST ENVIRONMENT  ####"
	echo "################################################################"
	echo "#### WARNING:             THERE IS NOT WARRANTY             ####"
	echo "################################################################"
	echo && echo && echo
	read -p "Sure to continue? [ YES | NO ]: " RESPOSTA
	case "${RESPOSTA}" in
		YES)
			echo "Accepted!"
			;;
		*)
			echo "Aborting!"
			echo && echo
			exit 3
			;;
	esac

	NEWROOT_PART_MODE_OPTION="lvm-small"
	GRUB_TEMPLATE_NAME="grub.cfg_template-bootgrub_part-lvm-and-gpt-part"

	## Get disk info
	fn_get_disk_info

	## Config lvm2 needed vars
	fn_lvm2_config_vars

	## Crear tar rootfs original i recrea particions
	fn_partitions_recreation

	## Creació dispositius lvms
	fn_lvm2_creation

	## Get VG id and LV id
	fn_lvm2_get_lvmids

	## Reparació part original rootfs
	fn_oldroot_part_repair

	## Set UUID definitiu a newroot
	fn_newroot_conf_uuid

	## Config boot
	fn_rootfs_migrate_conf_boot

	## Grow LV and fs on lv-rootfs si mode lvm-full
	if [ "${NEWROOT_PART_MODE_OPTION}" == "lvm-full" ]; then
		echo && echo "Verificant FS a \"${LV_DEV_VG_FULL_PATH}\"..."
		e2fsck -f "${LV_DEV_VG_FULL_PATH}"
		echo && echo "Extenent LV i FS de \"${LV_DEV_VG_FULL_PATH}\"..."
		lvresize -v -l 100%FREE -r "${LV_DEV_VG_FULL_PATH}"
		#resize2fs "${LV_DEV_MAPPER_PATHNAME}"
		#e2fsck -f "${LV_DEV_MAPPER_PATHNAME}"
	fi

	echo && echo "Rootfs migration process finished!"
	echo
	echo "- The \"oldroot\" filesystem has been moved to \"${NEWROOT_DEV_FULL_PATH}\""
       	echo "- A boot partition has been created as seprated boot partition, and will be mounted on the \"newroot\" /boot dir"
       	echo && echo "FINISHED: Now you can reboot and the rootfs should be boot from the migrated LV!!"
	echo && echo
}


fn_rootfs_migrate_conf_boot() {
## Si grub files a "gpt part"
   ## grub.cfg
      ## no posar --set-root abans del search
      ## search sense --hint
## Si grub files a "lv"
   ## grub.cfg
      ## si posar --set-root alb lvmid/'...' abans del search
      ## search amb --hint=lvmid/'...'

	## Configurar boot lv-rootfs
	## Mount oldroot
	#mount -v  "${OLDROOT_DEV_FULL_PATH}" "${OLDROOT_MOUNT_POINT}"
	## Mount newroot
	echo && echo "Mounting \"newroot\"..."
	mount -v "${NEWROOT_DEV_FULL_PATH}" "${NEWROOT_MOUNT_POINT}"
	## Provisional bootgrub part mount to copy boot files from newroot
	echo && echo "Provisional bootgrub part mount to copy boot files from newroot..."
	[ ! -d "${BOOT_GRUB_TMP_MOUNT_POUINT}" ] && mkdir -v "${BOOT_GRUB_TMP_MOUNT_POUINT}"
	mount -v "${BOOT_GRUB_DEV_FULL_PATH}" "${BOOT_GRUB_TMP_MOUNT_POUINT}"
	## Copy newroot boot files to bootgrub mounted part
	echo && echo "Copying boot files from newroot to bootgrub partition..."
	rsync -a "${NEWROOT_MOUNT_POINT}"/boot/ "${BOOT_GRUB_TMP_MOUNT_POUINT}"/
	echo && echo "Eliminant contingut de dir boot de newroot després de copiar-ho a bootgrub part..."
	rm -r "${NEWROOT_MOUNT_POINT}"/boot/*
	sync
	## Umount provisional bootgrub part mount point
	echo && echo "Umounting provisional \"bootgrub\" part mount point..."
	umount -v "${BOOT_GRUB_TMP_MOUNT_POUINT}"
	## Final bootgrup part mount
	echo && echo "Mounting \"bootgrub\" part on final mount point..."
	mount -v "${BOOT_GRUB_DEV_FULL_PATH}" "${NEWROOT_MOUNT_POINT}"/boot
	## Mount efi partition
	echo && echo "Mounting \"efi part\"..."
	mount -v "${BOOT_EFI_DEV_FULL_PATH}" "${NEWROOT_MOUNT_POINT}"/boot/efi
	INITR_IMAGE=$(find "${NEWROOT_MOUNT_POINT}"/boot -name "initr*" | awk -F'/' '{print $NF}')
	KRNL_IMAGE=$(find "${NEWROOT_MOUNT_POINT}"/boot  \( -name "vmlinu*" -o -name "linu*" \) \
		| grep --invert-match "grub" | awk -F'/' '{print $NF}')
	echo
	echo "INITR_IMAGE = ${INITR_IMAGE}"
	echo "KRNL_IMAGE = ${KRNL_IMAGE}"

	# Get and config grub.cfg template
	echo && echo "Obtenint \"${GRUB_TEMPLATE_NAME}\"..."
	curl \
	 https://gitlab.com/berbascum/berb-pivot-rootfs-migrator/-/raw/${TOOL_BRANCH}/${GRUB_TEMPLATE_NAME}?ref_type=heads \
	 -o "${NEWROOT_MOUNT_POINT}/boot/grub/grub.cfg"

	## Replace ids in orig rootfs part grub.cfg with 2 menuentry (LVM and GPT Part)
	echo && echo "Configurant \"${GRUB_TEMPLATE_NAME}\"..."
	sed -i "s/NEWROOT_FS_UUID_REPLACE/${NEWROOT_FS_UUID}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s/OLDROOT_FS_UUID_REPLACE/${OLDROOT_FS_UUID}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s/BOOT_GRUB_PART_FS_UUID_REPLACE/${BOOT_GRUB_PART_FS_UUID}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s/VG_ID_REPLACE/${VG_UUID}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s/LV_ID_REPLACE/${LV_UUID}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s|LV_DEV_MAPPER_PATHNAME_REPLACE|${LV_DEV_MAPPER_PATHNAME}|g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s/INITR_IMAGE_REPLACE/${INITR_IMAGE}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	sed -i "s/KRNL_IMAGE_REPLACE/${KRNL_IMAGE}/g" "${NEWROOT_MOUNT_POINT}"/boot/grub/grub.cfg
	## Replace uuid on efi partition grub.cfg
	echo && echo "Configuring grub.cfg from efi partition..."
	sed -i "s/${OLDROOT_FS_UUID}/${BOOT_GRUB_PART_FS_UUID}/g" "${NEWROOT_MOUNT_POINT}"/boot/efi/EFI/debian/grub.cfg

	# Config lv-rootfs fstab on newroot
	echo && echo "Configurant fstab de lv-rootfs..."
	cp "${NEWROOT_MOUNT_POINT}"/etc/fstab "${NEWROOT_MOUNT_POINT}"/etc/fstab_backup
	echo "# /etc/fstab: static file system information" > "${NEWROOT_MOUNT_POINT}"/etc/fstab
	echo "UUID=${NEWROOT_FS_UUID} / ext4 rw,discard,errors=remount-ro,x-systemd.growfs 0 1" \
							      		>> "${NEWROOT_MOUNT_POINT}"/etc/fstab
	echo "UUID=${BOOT_GRUB_PART_FS_UUID} /boot ext4 defaults 0 0" >> "${NEWROOT_MOUNT_POINT}"/etc/fstab
	echo "UUID=${BOOT_EFI_PART_FS_UUID} /boot/efi vfat defaults 0 0" >> "${NEWROOT_MOUNT_POINT}"/etc/fstab
	echo && echo "cat de fstab"
	cat "${NEWROOT_MOUNT_POINT}"/etc/fstab
	echo && echo "cat de fstab_backup"
	cat "${NEWROOT_MOUNT_POINT}"/etc/fstab_backup
	
	## Iniciant chroot per a executar grub-install
	echo && echo "Starting chroot on lv-rootfs to reinstall grub..."
	fn_chroot start
	echo
	chroot "${CHROOT_DIR}" /sbin/grub-install --no-floppy "${OLDROOT_DEV_PATH}"
	sleep 1
	## Aturant chroot
	echo && echo "Stopping chroot..."
	fn_chroot stop
	sleep 1

	## Desmunta parts
	sync
	sleep 1
	umount -v "${NEWROOT_MOUNT_POINT}"/boot/efi
	sleep 1
	umount -v "${NEWROOT_MOUNT_POINT}"/boot
	sleep 1
	umount -v "${NEWROOT_MOUNT_POINT}"
}

fn_grub_reinstall() {
	## Muntar sda2 i part efi sda15 a boot/efi de sda2
	   mount /dev/sda2 /mnt/sda2
	   mount /dev/sda15 /mnt/sda2/boot/efi
	## Executar chroot a sda2 per reinstal.lar grub
	   berb-chroot.sh start
	   grub-install /dev/sda   
	   update-grub2    
	   # exit chroot
	   sync
	   cd /
	   berb-chroot.sh stop
	   umount -v /mnt/sda2/boot/efi
	   umount -v /mnt/sda2
}

##################
## Inici script ##
##################

## Funcions creades NO UTILITZADES
## fn_systemd_agresive_reload
## fn_grub_reinstall

## Si script-arg1 és rootfs-mograte-lvm-small, s'executa la funció i se surt de l'script
if [ "$1" == "rootfs-migrate-lvm-small" ]; then
	fn_rootfs_migrate_lvm_small
	exit 0
fi

if [ "$1" == "pivot-root" ]; then
	echo > /dev/null
	## Si no existeix arxiu de pivot stage1 done executa stage1 i surt
	if [ ! -e "${NEWROOT_DIR}/pivot_stage1" -a ! -e "/pivot_stage1" ]; then
	       fn_pivot_stage1
	       exit 0
	fi
	if [ ! -e "/pivot_stage2" ]; then
	       fn_pivot_stage2
	       exit 0
	fi
	if [ ! -e "/pivot_stage3" ]; then
	       fn_pivot_stage3
	       exit 0
	fi
	if [ ! -e "/pivot_stage_last" ]; then
	       fn_pivot_stage_last
	       exit 0
	fi
fi

